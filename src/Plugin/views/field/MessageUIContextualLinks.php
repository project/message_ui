<?php

namespace Drupal\message_ui\Plugin\views\field;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\message_ui\MessageUiViewsContextualLinksManager;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Presenting contextual links to the messages view.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("message_ui_contextual_links")
 */
final class MessageUIContextualLinks extends FieldPluginBase {

  /**
   * Stores the result of message_view_multiple for all rows to reuse it later.
   *
   * @var array
   */
  protected $build;

  /**
   * The contextual links manager.
   *
   * @var \Drupal\message_ui\MessageUiViewsContextualLinksManager
   */
  protected $contextualLinksManager;

  /**
   * MessageUIContextualLinks constructor.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param array $cache
   *   The cache.
   * @param \Drupal\message_ui\MessageUiViewsContextualLinksManager $contextualLinksManager
   *   The contextual links manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $cache, MessageUiViewsContextualLinksManager $contextualLinksManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->contextualLinksManager = $contextualLinksManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      [],
      $container->get('plugin.manager.message_ui_views_contextual_links')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $contextual_links = $this->contextualLinksManager;

    $links = [];

    // Iterate over the plugins.
    foreach ($contextual_links->getDefinitions() as $plugin) {
      /** @var \Drupal\message_ui\MessageUiViewsContextualLinksInterface $contextual_link */
      $contextual_link = $contextual_links->createInstance($plugin['id']);
      $contextual_link->setMessage($values->_entity);

      $access = $contextual_link->access();
      if ($access instanceof AccessResultInterface) {
        $access = $access->isAllowed();
      }

      if (!$access || !$link = $contextual_link->getRouterInfo()) {
        // Nothing happens in the plugin. Skip.
        continue;
      }

      $link['attributes'] = ['class' => [$plugin['id']]];

      $links[$plugin['id']] = $link + ['weight' => $plugin['weight']];
    }

    usort($links, ['Drupal\Component\Utility\SortArray', 'sortByWeightElement']);

    $row['operations']['data'] = [
      '#type' => 'operations',
      '#links' => $links,
    ];

    return $row;
  }

}
